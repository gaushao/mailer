// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import nodemailer, { TransportOptions } from 'nodemailer'

export type EmailRequest = {
  name: string
  address: string
  message: string
  to: string
  subject: string
}

export type TransporterConfig = {
  host: string
  port: string
  user: string
  pass: string
}

const { EMAIL_HOST, EMAIL_PORT, EMAIL_USER, EMAIL_PASS } = process.env
const config: TransporterConfig = {
  host: EMAIL_HOST,
  port: EMAIL_PORT,
  user: EMAIL_USER,
  pass: EMAIL_PASS,
}

function createTransporter() {
  const { host, port, user, pass } = config
  return nodemailer.createTransport({
    host,
    port,
    secure: false,
    auth: { user, pass },
  } as TransportOptions)
}
function sendEmail(req: EmailRequest) {
  const { name, address, message, to, subject } = req
  const transporter = createTransporter()
  const emailConfig = {
    from: address,
    to,
    subject,
    text: message + '\n\nName: ' + name + '\nAddress: ' + address,
  }
  return new Promise((resolve, reject) => {
    try {
      transporter.sendMail(emailConfig, (error, info) => (error ? reject(error) : resolve(info)))
    } catch (error) {
      reject(error)
    }
  })
}

export default async function email(req: NextApiRequest, res: NextApiResponse) {
  res.send(await sendEmail(req.body.data))
}
