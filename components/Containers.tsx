export function Row({ children }) {
  return <span>{children}</span>
}
export function Col({ children }) {
  return <div>{children}</div>
}
export function Block({ children }) {
  return (
    <Col>
      <Row>{children}</Row>
    </Col>
  )
}
