import { useEffect, useState, useMemo } from 'react'
import useSendEmail from './hooks/useSendEmail'
import useAsync from './hooks/useAsync'
import { Col } from './Containers'
import { Name, Address, Message, Subject, To } from './Inputs'

function Form() {
  const [name, setName] = useState('')
  const [address, setAddress] = useState('')
  const [message, setMessage] = useState('')
  const [to, setTo] = useState('')
  const [subject, setSubject] = useState('')
  const req = useMemo(
    () => ({
      name,
      address,
      to,
      subject,
      message,
    }),
    [name, address, message, to, subject]
  )
  const sendEmail = useSendEmail(req)
  const { loading, result, call, reset, error } = useAsync<Response>(sendEmail)
  const [response, setResponse] = useState(null)
  const [exception, setException] = useState(null)
  const dismiss = () => {
    setResponse(null)
    setException(null)
  }
  const DismissButton = () => <button onClick={dismiss}>Back</button>
  const ResponseFeedback = ({ children }) => (
    <Col>
      <strong>{children}</strong>
      <br />
      <DismissButton />
    </Col>
  )
  useEffect(() => {
    if (result) {
      result
        .json()
        .then((data) => {
          setResponse(data)
          reset()
        })
        .catch((err) => {
          setException(err)
          reset()
        })
    }
  }, [result, reset, error])
  if (loading) return <strong>Loading...</strong>
  if (exception) {
    return <ResponseFeedback>error sending email</ResponseFeedback>
  }
  if (response) {
    return <ResponseFeedback>email sent to {to}</ResponseFeedback>
  }
  return (
    <Col>
      <strong>Mailer</strong>
      <Name value={name} onChange={setName} />
      <Address value={address} onChange={setAddress} />
      <strong>Mail</strong>
      <To value={to} onChange={setTo} />
      <Subject value={subject} onChange={setSubject} />
      <Message value={message} onChange={setMessage} />
      <button onClick={call}>Submit</button>
    </Col>
  )
}

export default Form
