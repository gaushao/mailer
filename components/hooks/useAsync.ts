import { useCallback, useMemo, useState } from 'react'

const useAsync = <T>(func: () => Promise<T>) => {
  const [loading, setLoading] = useState(false)
  const [result, setResult] = useState<T | null>(null)
  const [error, setError] = useState<Error | null>(null)
  const reset = useCallback(() => {
    setResult(null)
    setLoading(false)
    setError(null)
  }, [])
  const handle = useCallback(
    (cb) => (data) => {
      reset()
      cb(data)
    },
    [reset]
  )
  const resolve = handle(setResult)
  const reject = handle(setError)
  const call = useCallback(() => {
    setLoading(true)
    func().then(resolve).catch(reject)
  }, [func, resolve, reject])
  return useMemo(
    () => ({ loading, result, error, call, reset }),
    [loading, result, error, call, reset]
  )
}

export default useAsync
