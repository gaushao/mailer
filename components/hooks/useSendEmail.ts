import { useCallback, useMemo } from 'react'
import { EmailRequest } from '../../pages/api/email'

export type SendEmailFn = () => Promise<Response>
export type UseSendEmail = (req: EmailRequest) => SendEmailFn

const useSendEmail: UseSendEmail = (req) => {
  const body = useMemo(() => JSON.stringify({ data: req }), [req])
  return useCallback(
    () =>
      fetch('/api/email', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body,
      }),
    [body]
  )
}

export default useSendEmail
