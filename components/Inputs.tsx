import { ChangeEvent, useState, useCallback } from 'react'
import { Col, Row, Block } from './Containers'

function Field({ label, children }) {
  return (
    <Block>
      {label}:&nbsp;
      {children}
    </Block>
  )
}
function TextInput({ value, onChange }) {
  const onChangeTextInput = (e: ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.value)
  }
  return <input type="text" value={value} onChange={onChangeTextInput} />
}
function TextArea({ value, onChange }) {
  const onChangeTextArea = (e: ChangeEvent<HTMLTextAreaElement>) => {
    onChange(e.target.value)
  }
  return <textarea value={value} onChange={onChangeTextArea} />
}

function TextInputField({ label, value, onChange }) {
  return (
    <Field label={label}>
      <TextInput value={value} onChange={onChange} />
    </Field>
  )
}
function TextAreaField({ label, value, onChange }) {
  return (
    <Field label={label}>
      <TextArea value={value} onChange={onChange} />
    </Field>
  )
}

export function Name({ value, onChange }) {
  return <TextInputField label="Name" value={value} onChange={onChange} />
}
export function Address({ value, onChange }) {
  return <TextInputField label="Address" value={value} onChange={onChange} />
}
export function To({ value, onChange }) {
  return <TextInputField label="Address" value={value} onChange={onChange} />
}
export function Subject({ value, onChange }) {
  return <TextInputField label="Subject" value={value} onChange={onChange} />
}
export function Message({ value, onChange }) {
  return <TextAreaField label="Message" value={value} onChange={onChange} />
}
